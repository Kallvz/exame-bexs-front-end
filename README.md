# exame-bexs-front-end

Exame bextech feito por guilhermekalel@gmail.com

## Notas

- Desenvolvido com React.
- A interface é responsiva.
- Testado apenas no Chrome!
- O cartão é animado (mouse over e ao selecionar o campo CVV).
- Procurei utilizar o mínimo possivel de componentes e ou soluções de terceiros, optando por fazer a maior parte por mim mesmo.
- O foco foi implementar o design da tela o mais rápido possivel, pára tanto alguns atalhos foram tomados, como por exemplo o breadcrumb que é um mock.
- O 'chevron down' do campo de select continua vermelho mesmo após a seleção seguindo a especificação do design.
- Não estava certo de qual a hora certa para trocar o fundo do cartão, achei que faria mais sentido trocar apenas quando se começa a digitar o número.
- Seguindo exemplo do design, a data está em ANO/MES.
- Não entendi bem o que era esperado na parte da 'camada que chama um REST' mas com os campos válidos o botão continue faz um POST em 'fakeserver/pagar'.

## Para testar

 `$ yarn install `
 
 ou

 `$ npm install `

 e então
 `$ yarn start`




 ##### by Guilherme Kalel