import React from 'react';

import './icon-button.css';

const defaultOnClick = () => {console.log('btn without onCLick function!')};

function IconButton(props) {

    const { onClick } = props;

    const fnOnClick =  onClick ? onClick : defaultOnClick;

    return (
        <div id={props.id} className="container-style" onClick={fnOnClick} style={props.style} >

            {(props.iconLeft && ( 
                <div className="text-style" style={{margin: 0, ...props.textStyle}} >{props.iconLeft}</div> 
            ))} 

            <div className="text-style" style={props.textStyle} >
                {props.text}
            </div>

            {(props.iconRight && ( 
                <div className="text-style" style={{margin: 0, ...props.textStyle}} >{props.iconRight}</div> 
            ))} 

        </div>
    );
}

export default IconButton;
