import React from 'react';
import {IMaskInput} from 'react-imask';

import './input-required.css'


function InputRequired(props) {

    const {
        label, value, onChange, 
        name, hasError, errorMessage, 
        mask, onFocus, onBlur,
        disableError,
    } = props;

    const mergedStyles = "container-input " + props.classStyle;

    const lineStyle = {
        width: '100%',
        borderBottom : (hasError && !disableError) ? '1px solid #EB5757' : '1px solid #C9C9C9'
    }

    return (
        <div className={mergedStyles} style={props.style}>

            <div style={{minHeight: '24px'}} >
                {(value && (
                    <label className="label"> {label} </label>
                ))}
            </div>
             
            <IMaskInput
                className="IMaskInput"
                mask={mask}
                name={name} 
                placeholder={label}
                value={value} 
                onFocus={onFocus}
                onBlur={onBlur}
                unmask={'typed'}
                onAccept={
                    (value, mask) => {
                        // console.log(value)
                        onChange({name, value});
                    }
                }
            />

            <span style={lineStyle} />

            <div className="error-box" >
                {(!disableError && errorMessage && hasError &&( 
                    <label className="error-message" > {errorMessage} </label>
                ))}
            </div>
        

        </div>
    );
}

export default InputRequired;
