import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronDown } from '@fortawesome/free-solid-svg-icons'

import './select-required.css';


function InputRequired(props) {

    const {disableError, label, value, onChange, name, hasError, errorMessage, options} = props;

    let mergedStyles = "container " + props.classStyle;

    const lineStyle = {
        width: '100%',
        borderBottom : (hasError && !disableError) ? '1px solid #EB5757' : '1px solid #C9C9C9'
    }

    const selectStyle = {
        fontSize: '17px',
        backgroundColor: 'transparent',
        color: (value) ? '#3C3C3C' : '#C9C9C9',  
    }

    return (
        <div className={mergedStyles} >

            <select id={name} name={name}
                value={value} 
                onChange={({target}) => onChange({name, value: target.value})} 
                style={selectStyle}
            >
                <option value="">{label}</option>
                {(options && options.map(({id, desc})=> (<option key={id} value={id}>{desc}</option>) ))}

            </select>

            <span style={lineStyle} />

            <div className="error-box" >
                {(errorMessage && hasError && !disableError &&( 
                    <label className="error-message"> {errorMessage} </label>
                ))}
            </div>

            <FontAwesomeIcon icon={faChevronDown} className="chevron" />

        </div>
    );
}

export default InputRequired;
