import React from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Loja from './loja/Loja';


ReactDOM.render(
  <React.StrictMode>
     <Router>
      <Switch>
        <Route path="/"> <Loja /> </Route>
      </Switch>
     </Router>
  </React.StrictMode>,
  document.getElementById('root')
);
