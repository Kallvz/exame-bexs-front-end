import React, {useState} from 'react';
import { Button } from 'react-bootstrap';
import Pagamento from '../pagamento/Pagamento';
import './loja.css';


function Loja() {

    const [showBoxPagto, setShowBoxPagto] = useState(true);

    return (
        <div className="loja">

            {(!showBoxPagto && (
                <>
                    <img src="logo192.png" className="loja-logo" alt="logo" />
                    <p>
                        Bem-vindo à loja!!
                    </p>
                    
                    <Button onClick={()=> setShowBoxPagto(!showBoxPagto)}>
                        Clique aqui!!
                    </Button>
                </>
            ))} 

            {(showBoxPagto && (
                <div className="boxPagto">
                    <Pagamento />
                </div>
            ))}


        </div>
    );
}

export default Loja;
