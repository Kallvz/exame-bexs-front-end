import React, {useState, useReducer} from 'react';
import IconButton from '../global-components/iconButton/IconButton'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronLeft, faChevronRight, faCheckCircle } from '@fortawesome/free-solid-svg-icons'

import CartaoCred from './components/cartao/CartaoCred'
import FormPagamento from './components/formPagamento/FormPagamento'

import { Post } from "../services/Services"

import './pagamento.css';

const initialFormState = {
    formTouched: false,
    numCartao: '',
    nome: '',
    validade: '',
    cvv: '',
    qtdParcelas: ''
};

function formReducer (state, {field, value}){
    return { ...state, formTouched: true, [field]: value }
}

function submitForm (formData){
    console.log('submit')

    Post(formData).then(data => {
            console.log(data);
        }).catch(error => {
            console.log(error);
        });
} 

function Pagamento() {
    
    const [formState, formDispatch] = useReducer(formReducer, initialFormState);
    const [showVerso, setShowVerso] = useState(false);

    const { numCartao, nome, validade, cvv } = formState;

    return (
        <div className="pagamento">

            <div className="wrapper-cartao">
                
                <div id="btnVoltarPortrait" >
                    <IconButton 
                        iconLeft={<FontAwesomeIcon icon={faChevronLeft} /> } 
                        style={{position: 'absolute', left: 0 }}
                        textStyle={{
                            fontSize: '20px', 
                        }}
                        onClick={()=> {console.log('clicou!')}} 
                    />

                    <b>Etapa 2</b> <span>&nbsp;&nbsp;</span> de 3
                </div>
                
                <IconButton 
                    id="btnAddCardPortrait"
                    iconLeft={<img src="ico-cartao-cicle.svg" alt="cart" /> } 
                    text="Adicione um novo cartão de crédito" 
                    style={{marginTop: '30px', width: '220px'}}
                    textStyle={{
                        color: 'white', 
                        fontSize: '16px', 
                        fontWeight: 'bold', 
                        textAlign: 'left'
                    }}
                />

                <IconButton 
                    id="btnVoltarLandscape"
                    iconLeft={<FontAwesomeIcon icon={faChevronLeft} /> } 
                    text="Alterar forma de pagamento" 
                    style={{color: 'white', marginTop: '50px'}}
                    onClick={()=> {console.log('clicou!')}} 
                />
                
                <IconButton 
                    id="btnAddCardLandscape"
                    iconLeft={<img src="ico-cartao-cicle.svg" alt="cart" /> } 
                    text="Adicione um novo cartão de crédito" 
                    style={{marginTop: '50px'}}
                    textStyle={{
                        color: 'white', 
                        fontSize: '20px', 
                        fontWeight: 'bold', 
                        textAlign: 'left', 
                        marginLeft: '5px'
                    }}
                />
                
                <CartaoCred 
                    numCartao={numCartao} 
                    nome={nome}
                    validade={validade}
                    cvv={cvv} 
                    showVerso={showVerso}
                />

            </div>


            <div className="wrapper-form">

                <div >
                    <div className="mock-breadcrumb" >
                        <FontAwesomeIcon className="step-icon" icon={faCheckCircle} />
                        Carrinho

                        <FontAwesomeIcon icon={faChevronRight} />

                        <div className="step-decorator"> 2 </div>
                        Pagamento

                        <FontAwesomeIcon icon={faChevronRight} />

                        <div className="step-decorator"> 3 </div>
                        Confirmação
                    </div>


                    <FormPagamento 
                        formState={formState} 
                        formDispatch={formDispatch} 
                        fnSubmit={submitForm} 
                        setShowVerso={setShowVerso}
                    />
                </div>

            </div>

        </div>
    );
}

export default Pagamento;
