import React from 'react';

import Visa from '../assets/VISA.png';
import Card from '../assets/cartao-background.svg';
import CardEmpty from '../assets/cartao-background-empty.svg';
import CardVerse from '../assets/cartao-background-verse.svg';
import CardVerseEmpty from '../assets/cartao-background-verse-empty.svg';

import './cartao-cred.css';


//364 / 223

function CartaoCred(props) {

    const { showVerso, numCartao, nome, validade, cvv} = props;
    
    const emptyCard = (!numCartao && !nome && !validade && !cvv)
    
    const opt = !numCartao ? CardEmpty : Card;
    const optVerse = !numCartao ? CardVerseEmpty : CardVerse;

    const numero = emptyCard ? '****  ****  ****  ****' : numCartao;

    const card = document.getElementById('cartao-de-credito');
    if(card ) showVerso ? card.classList.add("hover") : card.classList.remove("hover") ;

    return (
        <div id="cartao-de-credito" className="container-cartao-style" style={props.style} >

            <div className="flip-card" >

                <div className="cartao-style cartao-front" 
                    style={{background: `transparent url(${opt}) 0% 0% no-repeat padding-box`}} 
                >

                    {(numCartao && (<img src={Visa} className="logo-visa" alt="" /> ))}

                    <div className="card-number-wrapper">
                        { numero.split('  ').map(
                            (num, k) =>  (<div className="card-number" key={k} > {num}</div>) 
                        )}
                    </div>

                    <div className="card-name" >
                        { !nome && emptyCard ? 'NOME DO TITULAR' : nome.toUpperCase() }
                    </div>

                    <div className="card-valid" >
                        { emptyCard ? '00/00' : validade }
                    </div>
            
                </div>


                <div className="cartao-style cartao-verse" 
                    style={{background: `transparent url(${optVerse}) 0% 0% no-repeat padding-box`}} 
                >

                    <div className="card-CVV" >{ !cvv ? '***' : cvv }</div>
                    
                </div>

            </div>


        </div>
    );
}

export default CartaoCred;
