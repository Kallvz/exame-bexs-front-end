import React, {useState} from 'react';
import { Row, Col } from 'react-bootstrap';
import InputRequired from '../../../global-components/input-required/InputRequired'
import SelectRequired from '../../../global-components/select-required/SelectRequired'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes} from '@fortawesome/free-solid-svg-icons'

import './form-pagto.css';


function validadeForm(formState){

    if(formState.numCartao.length !== 22) return false;
    if(formState.cvv.length !== 3) return false;
    if(formState.qtdParcelas === "") return false;

    const [ano, mes] = formState.validade.split('/');
    if(!ano || ano.length < 2 || Number(ano) < 20) return false;
    if(!mes || mes.length < 2 || Number(mes) > 12) return false;
    
    const nameArr = formState.nome.split(' ');;
    if((nameArr.length < 2 || nameArr[1] === "")) return false;

    return true;
}

function FormPagamento(props) {

    const {formState, formDispatch, fnSubmit} = props;
    const [showAlert, setShowAlert] = useState(false);
    
    const onChangeForm = ({name, value}) => {
        formDispatch({field: name, value: value });
    }

    const functionShowAlert = () => {
        clearTimeout();
        setShowAlert(true);
        setTimeout(() => {
            setShowAlert(false);
        }, 1.7 * 1000);

    }

    const {setShowVerso} = props;

    const { formTouched, numCartao, nome, validade, cvv, qtdParcelas } = formState;

    return (
        <form className="form-dados" onSubmit={(e) => e.preventDefault() }>

            {(showAlert && (
                <div className="div-alert" onClick={() => setShowAlert(false)} >
                    Verifique os campos, há erros!

                    <FontAwesomeIcon 
                        icon={faTimes} 
                        className="close-alert" 
                        onClick={() => setShowAlert(false)} 
                    />
                </div>
            ))}

            <InputRequired 
                label="Número do cartão"
                errorMessage="Número do cartão inválido!" 
                mask="0000  0000  0000  0000"
                value={numCartao} 
                onChange={onChangeForm}
                name="numCartao"
                hasError={numCartao.length !== 22}
                disableError={!formTouched}
            />

            <InputRequired 
                classStyle="rowDoubleField"
                label="Nome (igual ao cartão)"
                mask={/^.{0,26}$/}
                errorMessage="Insira seu nome completo" 
                value={nome} 
                onChange={onChangeForm}
                name="nome"
                hasError={(()=> {
                    const nameArr = nome.split(' ');
                    return (nameArr.length < 2 || nameArr[1] === "");
                })()}
                disableError={!formTouched}
            />

            <Row className="rowDoubleField" >
                <Col xs={6} >
                    <InputRequired 
                        label="Validade"
                        errorMessage="Data inválida" 
                        mask={'00/00'}
                        value={validade} 
                        onChange={onChangeForm}
                        name="validade"
                        hasError={(()=> {
                            const [ano, mes] = validade.split('/');
                            if(!ano || ano.length < 2 || Number(ano) < 20) return true;
                            if(!mes || mes.length < 2 || Number(mes) > 12) return true;
                            return false;
                        })()}
                        disableError={!formTouched}
                    />
                </Col>
                
                <Col xs={6}>    
                    <InputRequired 
                        label="CVV"
                        mask="000"
                        errorMessage="Código inválido" 
                        value={cvv} 
                        onChange={onChangeForm}
                        name="cvv"
                        hasError={cvv.length !== 3}
                        onFocus={() => setShowVerso(true)}
                        onBlur={() => setShowVerso(false)}
                        disableError={!formTouched}
                    />
                </Col>
            </Row>

            <SelectRequired 
                label="Número de parcelas"
                errorMessage="Insira o número de parcelas" 
                value={qtdParcelas} 
                onChange={onChangeForm}
                name="qtdParcelas"
                hasError={qtdParcelas === ''}
                classStyle="select-parcelas"
                options={[
                    {id:'1', desc: '1x R$ 3.000,00'},
                    {id:'2', desc: '2x R$ 1.500,00'},
                    {id:'3', desc: '3x R$ 1.000,00'},
                ]}
                disableError={!formTouched}
            />

            <button className="btn-continuar" 
                onClick={()=> {
                    validadeForm(formState) ? fnSubmit(formState) : functionShowAlert();
                }}>
                Continuar
            </button>

        </form>
    );
}

export default FormPagamento;
