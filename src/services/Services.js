import axios from 'axios';

export function Post(data){

    // const payload = JSON.stringify({...data});

    return new Promise((resolve, reject) => {

        axios.post(`fakeserve/pagar`, data).then(r => {
            
            resolve(JSON.parse(r));

        }, error => {
            reject(error);
        })

    });

}

